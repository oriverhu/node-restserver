const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

let categoriaSchema = new Schema({
    descripcion: {
        type: String,
        unique: true,
        required: [true, 'La descripción es obligatoria']
    },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    estado: { type: Boolean, default: true }
});
categoriaSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })
module.exports = mongoose.model('Categoria', categoriaSchema);

// const mongoose = require('mongoose');
// const uniqueValidator = require('mongoose-unique-validator');

// let Schema = mongoose.Schema;


// let categoriaSchema = new Schema({
//     nombre: {
//         type: String,
//         // unique: true,
//         required: [true, 'El nombre es obligatorio']
//     },
//     estado: {
//         type: Boolean,
//         default: true
//     },
// });

// categoriaSchema.methods.toJSON = function() {
//     let categ = this;
//     let categObject = categ.toObject();
//     delete categObject.password;

//     return categObject;
// }

// categoriaSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

// module.exports = mongoose.model('Categoria', categoriaSchema)