const jwt = require('jsonwebtoken');

//================================== 
//   Verificar Token
//================================== 
let verificaToken = (req, res, next) => {
    let token = req.get('token');

    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            res.status(401).json({
                ok: false,
                mensaje: 'Token inválido  \n ' + err
            });
        }
        req.usuario = decoded.usuario;

        next();
    })
};

//================================== 
//   Verificar Rol
//================================== 
let verificaRol = (req, res, next) => {
    let usuario = req.usuario;

    if (usuario.role === 'ADMIN_ROLE') {
        next();
        return;
    } else {
        return res.json({
            ok: false,
            err: {
                mensaje: 'El usuario no es administrador'
            }
        });
    }
};

let verificaTokenImg = (req, res, next) => {
    let token = req.query.token;
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            res.status(401).json({
                ok: false,
                mensaje: 'Token inválido  \n ' + err
            });
        }
        req.usuario = decoded.usuario;
        next();
    })
}

module.exports = {
    verificaToken,
    verificaRol,
    verificaTokenImg
}