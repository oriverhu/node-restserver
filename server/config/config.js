//================================== 
//   Puerto
//================================== 

process.env.PORT = process.env.PORT || 3000;

//================================== 
//   Entorno
//================================== 
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

//================================== 
//   Vencimiento del Token
//================================== 
// 60 segundos
// 60 minutos
// 24 horas
// 30 dias
process.env.CADUCIDAD = '48h';

//================================== 
//   SEED de autenticación
//================================== 
process.env.SEED = process.env.SEED || 'este-es-el-seed-de-desarrollo';

//================================== 
//   Base de datos
//================================== 

let urlBD

if (process.env.NODE_ENV === 'dev') {
    urlBD = 'mongodb://localhost:27017/cafe';
} else {
    urlBD = 'mongodb+srv://oriverhu:bpQ5IQXn28DELNHP@cluster0-um0ib.mongodb.net/cafe';
}

process.env.urlBD = urlBD;

//================================== 
//   Google Client ID 
//==================================

process.env.ClientId = '268472901259-c11tdkrnqdshhhu78m1g3npagv10opfm.apps.googleusercontent.com';