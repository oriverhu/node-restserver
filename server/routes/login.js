const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Usuario = require('./../models/usuario');
const app = express();

const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(process.env.ClientId);


app.post('/login', (req, res) => {

    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {

        if (err) {
            res.status(400).json({
                ok: false,
                mensaje: err
            });
            return;
        }

        if (!usuarioDB) {
            res.status(400).json({
                ok: false,
                mensaje: 'Usuario incorrecto'
            });
            return;
        }

        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            res.status(400).json({
                ok: false,
                mensaje: 'Contraseña incorrecta'
            });
            return;
        }

        let token = jwt.sign({
            usuario: usuarioDB
        }, process.env.SEED, { expiresIn: process.env.CADUCIDAD }); //30 dias

        res.json({
            ok: true,
            usuario: usuarioDB,
            token
        })

    })

})

//configuraciones de google
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.ClientId, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();
    return {
        nombre: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    };
}


app.post('/google', async(req, res) => {
    let token = req.body.idtoken;
    //mi work
    // verify(token)
    //     .then(resp => {
    //         res.json({
    //             usuario: resp
    //         });
    //     })
    //     .catch((e => {
    //         return res.status(403).json({
    //             ok: false,
    //             mensaje: e
    //         });
    //     }));

    let googleUser = await verify(token).catch(e => {
        return res.status(403).json({
            ok: false,
            mensaje: String(e)
        });
    })

    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
        if (err) {
            res.status(400).json({
                ok: false,
                mensaje: err
            });
            return;
        }

        if (usuarioDB) {
            if (usuarioDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Debe de usar su autenticación normal'
                });
            } else {
                let token = jwt.sign({
                    usuario: usuarioDB
                }, process.env.SEED, { expiresIn: process.env.CADUCIDAD }); //30 dias

                return res.json({
                    usuario: usuarioDB,
                    token
                });
            }
        } else {
            // si usuario no existe
            let usuario = new Usuario();

            usuario.nombre = googleUser.nombre;
            usuario.email = googleUser.email;
            usuario.img = googleUser.img;
            usuario.google = true;
            usuario.password = ':)';

            usuario.save((err, usuarioDB) => {
                if (err) {
                    return res.status(400).json({
                        ok: false,
                        mensaje: err
                    });
                }

                let token = jwt.sign({
                    usuario: usuarioDB
                }, process.env.SEED, { expiresIn: process.env.CADUCIDAD }); //30 dias

                return res.json({
                    usuario: usuarioDB,
                    token
                });
            })
        }
    })
})

module.exports = app;