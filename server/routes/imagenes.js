const express = require('express');
const app = express();
const { verificaTokenImg } = require('./../middlewares/autenticacion');

const path = require('path');

const Usuario = require('./../models/usuario');
const Producto = require('./../models/producto');

// app.get('/imagen/:tipo/:id', verificaToken, (req, res) => {
app.get('/imagen/:tipo/:id', verificaTokenImg, (req, res) => {

    let tipo = req.params.tipo.toLowerCase();
    let id = req.params.id;

    if (!id) {
        return res.status(400).json({
            ok: false,
            mensage: 'Debes enviar una Imagen.'
        });
    }

    let tiposValidos = ['productos', 'usuarios'];
    if (tiposValidos.indexOf(tipo) < 0) {
        return res.status(400).json({
            ok: false,
            mensage: 'Los tipos permitidos son: ' + tiposValidos.join(', ').toUpperCase()
        });
    }


    if (tipo == "usuarios") {
        Usuario.findById(id, (err, usuarioDB) => {
            if (err) {
                res.sendFile(path.resolve(__dirname, '../assets/not-found.jpg'));
                return;
            }

            if (!usuarioDB) {
                res.sendFile(path.resolve(__dirname, '../assets/not-found.jpg'));
                return;
            }

            if (!usuarioDB.img) {
                res.sendFile(path.resolve(__dirname, '../assets/not-found.jpg'));
                return;
            } else {
                res.sendFile(usuarioDB.img);
            }

        })
    } else {
        Producto.findById(id, (err, productoDB) => {
            if (err) {
                res.sendFile(path.resolve(__dirname, '../assets/not-found.jpg'));
                return;
            }

            if (!productoDB) {
                res.sendFile(path.resolve(__dirname, '../assets/not-found.jpg'));
                return;
            }

            if (!productoDB.img) {
                res.sendFile(path.resolve(__dirname, '../assets/not-found.jpg'));
                return;
            } else {
                res.sendFile(productoDB.img);
            }
        })
    }



})

module.exports = app;