const express = require('express');
const fileUpload = require('express-fileupload');
const { verificaToken } = require('./../middlewares/autenticacion');
const app = express();

const Usuario = require('./../models/usuario');
const Producto = require('./../models/producto');

const path = require('path');
const fs = require('fs');

app.use(fileUpload());

app.put('/upload/:tipo/:id', verificaToken, function(req, res) {

    let tipo = req.params.tipo.toLowerCase();
    let id = req.params.id;

    if (!id) {
        return res.status(400).json({
            ok: false,
            mensage: 'Debes enviar un ID.'
        });
    }

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok: false,
            mensage: 'No se ha seleccionado ningun archivo.'
        });
    }

    let tiposValidos = ['productos', 'usuarios'];
    if (tiposValidos.indexOf(tipo) < 0) {
        return res.status(400).json({
            ok: false,
            mensage: 'Los tipos permitidos son: ' + tiposValidos.join(', ').toUpperCase()
        });
    }

    let File = req.files.archivo;

    let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg'];
    let separator = File.name.split(".");
    let extension = separator[separator.length - 1];

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(400).json({
            ok: false,
            mensage: 'Las extensiones permitidas son: ' + extensionesValidas.join(', ').toUpperCase()
        });
    }

    let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`;

    File.mv('./uploads/' + tipo + '/' + nombreArchivo, function(err) {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensage: err
            });
        }


        let img = path.resolve(__dirname, '../../uploads/' + tipo + '/' + nombreArchivo);
        // console.log(img);


        if (tipo == "productos") {
            imagenProducto(id, img, res);
        } else {
            imagenUsuario(id, img, res);
        }


    });
})

function imagenUsuario(id, img, res) {

    Usuario.findById(id, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: err
            });
        }

        if (!usuarioDB) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Usuario no existe'
            });
        }

        borrarArchivo(usuarioDB.img);

        usuarioDB.img = img;

        usuarioDB.save((err, usuarioG) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: err
                });
            }
            res.json({
                ok: true,
                usuario: usuarioG
            });
        })
    })

    //mi work
    // Usuario.findByIdAndUpdate(id, { img }, { new: true, useFindAndModify: false }, (err, usuarioDB) => {
    //     if (err) {
    //         res.status(400).json({
    //             ok: false,
    //             mensaje: err
    //         });
    //     }

    //     res.json({
    //         ok: true,
    //         mensage: 'Archivo vinculado a ' + usuarioDB.nombre + ' y guardado en Usuarios'
    //     });
    // })
}

function imagenProducto(id, img, res) {

    Producto.findById(id, (err, productoDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: err
            });
        }

        if (!productoDB) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Producto no existe'
            });
        }

        borrarArchivo(productoDB.img)

        productoDB.img = img;

        productoDB.save((err, productoG) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: err
                });
            }
            res.json({
                ok: true,
                producto: productoG
            });
        })
    })

    //mi work
    // Producto.findByIdAndUpdate(id, { img }, { new: true }, (err, productoDB) => {
    //     if (err) {
    //         res.status(400).json({
    //             ok: false,
    //             mensaje: err
    //         });
    //     }

    //     res.json({
    //         ok: true,
    //         mensage: 'Archivo vinculado a ' + productoDB.nombre + ' y guardado en Productos'
    //     });
    // })
}

function borrarArchivo(img) {
    if (fs.existsSync(img)) {
        fs.unlinkSync(img);
    }
}

module.exports = app;