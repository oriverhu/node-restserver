const express = require('express');
const { verificaToken, verificaRol } = require('./../middlewares/autenticacion');
const Producto = require('./../models/producto');
const app = express();

app.get('/productos', verificaToken, (req, res) => {

    let desde = req.query.desde || 0;
    desde = Number(desde);
    let hasta = req.query.hasta || 5;
    hasta = Number(hasta);

    Producto.find({ disponible: true })
        .sort('nombre')
        .populate('usuario', 'nombre email')
        .populate('categoria', 'descripcion')
        .skip(desde)
        .limit(hasta)
        .exec((err, productos) => {
            if (err) {
                res.status(400).json({
                    ok: false,
                    mensaje: err
                });
            }

            Producto.countDocuments({}, (err, conteo) => {
                res.json({
                    ok: true,
                    productos,
                    conteo,
                    total: productos.length
                });
            })
        })
})

app.get('/productos/buscar/', verificaToken, (req, res) => {
    // parametro por url
    // let termino = req.params.termino;
    // let regex = new RegExp(termino, 'i');

    let body = req.body;
    let objeto = {};

    for (const prop in body) {

        if (
            prop != 'precioUni' &&
            prop != 'categoria' &&
            prop != 'nombre' &&
            prop != 'descripcion' &&
            prop != 'disponible' &&
            prop != 'usuario'
        ) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Al menos 1 de los parámetros es incorrecto [nombre,descripcion,precioUni,categoria,disponible,usuario]'
            });
        } else {
            if (prop != 'precioUni' && prop != 'categoria') {
                objeto[prop] = new RegExp(body[prop], 'i');
            } else {
                objeto[prop] = body[prop];
            }
        }
    }

    Producto.find(objeto)
        .populate('usuario', 'nombre email')
        .populate('categoria', 'descripcion')
        .exec((err, productos) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: err
                });
            }
            res.json({
                ok: true,
                productos
            });
        })
})

app.get('/producto/:id', verificaToken, (req, res) => {
    let id = req.params.id;

    Producto.findById(id)
        .populate('usuario', 'nombre email')
        .populate('categoria', 'descripcion')
        .exec((err, producto) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: err
                });
            }

            res.json({
                ok: true,
                producto,
            });
        })
})

app.post('/productos', [verificaToken], (req, res) => {

    let body = req.body;

    let producto = new Producto({
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        categoria: body.categoria,
        usuario: req.usuario._id
    });

    producto.save((err, productoDB) => {
        if (err) {
            res.status(400).json({
                ok: false,
                mensaje: err
            });
        } else {
            res.status(200).json({ producto: productoDB })
        }
    });
})
app.put('/productos/:id', [verificaToken], (req, res) => {
    let id = req.params.id;
    let body = req.body;

    Producto.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, productoDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        res.json({
            ok: true,
            producto: productoDB
        });
    })
})

app.delete('/productos/:id', [verificaToken, verificaRol], (req, res) => {
    let id = req.params.id;

    Producto.findByIdAndUpdate(id, { disponible: false }, { new: true }, (err, productoDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        res.json({
            ok: true,
            producto: productoDB
        });
    })

})


module.exports = app;