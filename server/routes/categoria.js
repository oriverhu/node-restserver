const express = require('express');
const { verificaToken, verificaRol } = require('./../middlewares/autenticacion');
const Categoria = require('./../models/categoria');
const app = express();

app.get('/categorias', verificaToken, (req, res) => {

    Categoria.find({ estado: true })
        .sort('descripcion')
        .populate('usuario', 'nombre email')
        .exec((err, categorias) => {
            if (err) {
                res.status(400).json({
                    ok: false,
                    mensaje: err
                });
            }

            Categoria.countDocuments({}, (err, conteo) => {
                res.json({
                    ok: true,
                    categorias,
                    conteo,
                    total: categorias.length
                });
            })
        })
})

app.get('/categoria/:id', verificaToken, (req, res) => {
    let id = req.params.id;

    Categoria.findById(id, (err, categoria) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        res.json({
            ok: true,
            categoria,
        });
    })
})

app.post('/categorias', [verificaToken], (req, res) => {

    let body = req.body;

    let categoria = new Categoria({
        descripcion: body.descripcion,
        usuario: req.usuario._id
    });

    categoria.save((err, categoriaDB) => {
        if (err) {
            res.status(400).json({
                ok: false,
                mensaje: err
            });
        } else {
            res.status(200).json({ categoria: categoriaDB })
        }
    });
})
app.put('/categorias/:id', [verificaToken], (req, res) => {
    let id = req.params.id;
    let body = req.body;

    Categoria.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, categoriaDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        res.json({
            ok: true,
            categoria: categoriaDB
        });
    })
})

app.delete('/categorias/:id', [verificaToken, verificaRol], (req, res) => {
    let id = req.params.id;

    Categoria.findByIdAndUpdate(id, { estado: false }, { new: true }, (err, categoriaDB) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        res.json({
            ok: true,
            categoria: categoriaDB
        });
    })

})


module.exports = app;